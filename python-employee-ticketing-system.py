# **********// Capstone Specifications //************#
# The goal of the capstone project is to create a simple employee ticketing system using classes and objects.

# 1. Create a Person class that is an abstract class that has the following methods:
# a. getFullName method
# b. addRequest method
# c. checkRequest method
# d. addUser method

from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        pass

# 2. Create an Employee class from Person with the following properties and methods:

# a. Properties (Make sure they are private and have getters/setters)
# i. firstName
# ii. lastName
# iii. email
# iv. department

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        
    # getters of Employee class
        
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName

    def get_email(self):
        return self._email
        
    def get_department(self):
        return self._department

        # setters of Employee class
    def set_firstName(self, firstName):
        self._firstName = firstName 

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email 

    def set_department(self, department):
        self._department = department 

    # b. Methods
    # c. Abstract methods (All methods just return Strings of simple text)
    # i. checkRequest() - placeholder method
    # ii. addUser() - placeholder method
    # iii. login() - outputs "<Email> has logged in"
    # iv. logout() - outputs "<Email> has logged out"

    # implementing the abstract methods from the parent class
    def checkRequest(self):
        pass

    def addUser(self):
        pass
    
    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def login(self):
        loginMail = f"{self._email} has logged in"
        return loginMail

    def logout(self):
        loginMail = f"{self._email} has logged out"
        return loginMail
    
    def addRequest(self):
       requestStat = "Request has been added"
       return requestStat



# 3. Create a TeamLead class from Person with the following properties and methods:
# a. Properties (Make sure they are private and have getters/setters)
# i. firstName
# ii. lastName
# iii. email
# iv. department

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._member = []
        
    # getters of TeamLead class
        
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName

    def get_email(self):
        return self._email
        
    def get_department(self):
        return self._department
    
    def get_members(self):
        return self._member

    # setters of TeamLead class
    def set_firstName(self, firstName):
        self._firstName = firstName 

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email 

    def set_department(self, department):
        self._department = department 

# b. Methods
# c. Abstract methods (All methods just return Strings of simple text)
# i. checkRequest() - placeholder method
# ii. addUser() - placeholder method
# iii. login() - outputs "<Email> has logged in"
# iv. logout() - outputs "<Email> has logged out"
# v. addMember() - adds an employee to the members list

# implementing the abstract methods from the parent class
    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def login(self):
        loginMail = f"{self._email} has logged in"
        return loginMail

    def logout(self):
        loginMail = f"{self._email} has logged out"
        return loginMail

    def addMember(self, employee):
        self._member.append(employee)
        return self._member

# 4. Create an Admin class from Person with the following properties and methods:
# a. Properties(make sure they are private and have getters/setters)
# i. firstName
# ii. lastName
# iii. email
# iv. department

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        
    # getters of Admin class
        
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName

    def get_email(self):
        return self._email
        
    def get_department(self):
        return self._department

    # setters of Admin class
    def set_firstName(self, firstName):
        self._firstName = firstName 

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email 

    def set_department(self, department):
        self._department = department 

# b. Methods
# c. Abstract methods (All methods just return Strings of simple text)
# i. checkRequest() - placeholder method
# ii. addUser() - placeholder method
# iii. login() - outputs "<Email> has logged in"
# iv. logout() - outputs "<Email> has logged out"
# v. addUser() - outputs "New user added”

# implementing the abstract methods from the parent class
    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def login(self):
        loginMail = f"{self._email} has logged in"
        return loginMail

    def logout(self):
        loginMail = f"{self._email} has logged out"
        return loginMail

    def addUser(self):
        addUserStat = "User has been added"
        return addUserStat

# 5. Create a Request class that has the following properties and methods:
# a. Properties
# i. name
# ii. requester
# iii. dateRequested
# iv. status

class Request():
    def __init__(self, name, requester, dateRequested):
        super().__init__()
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "Open"
        
    # getters of TeamLead class
        
    def get_name(self):
        return self._name

    def get_requester(self):
        return self._requester

    def get_dateRequested(self):
        return self._dateRequested
        
    def get_status(self):
        return self._status

    # setters of TeamLead class
    def set_name(self, name):
        self._name = name 

    def set_requester(self, requester):
        self._requester = requester

    def set_dateRequested(self, dateRequested):
        self._dateRequested = dateRequested 

    def set_status(self, status):
        self._status = status 

# b. Methods
# i. updateRequest
# ii. closeRequest
# iii. cancelRequest

    def addRequest(self):
       requestStat = "Request has been added"
       return requestStat

    def updateRequest(self):
        print("Request has been updated")

    def closeRequest(self):
        print(f"Request {self._name} has been closed")

    def cancelRequest(self):
        print("Request has been cancelled")

# Test Cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Administration")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New Hire Orientation", teamLead1, "27-Jul-2022")
req2 = Request("Laptop Repair", employee1, "1-Jul-2022")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
    if indiv_emp is not None:
        print(indiv_emp.getFullName())
    else:
        ''


assert admin1.addUser() == "User has been added"

req2.set_status("Closed")
print(req2.closeRequest())

